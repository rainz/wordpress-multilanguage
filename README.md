# WordPress Multilanguage Custom Script


## Description

A simple function that make your wordpress has multilanguage using only technique.

## Getting Started


### Dependencies

* WordPress 5.0 up

### Installing

* include "function_language" to your wordpress, you can using 3rd plugin ex. woody snippet.
* using custom script from "button_flag" place where your button language are.
* setting up wordpress permalink to "/%postname%/%category%/".
* create a page and name it to your master language remember this's a first page of your all page ex. "en".
* create a page and name it as you wish then setting page attribute change "Parent page" using "en".
* fine, this is 1 language so if you want 2 language please do it again but remember all child page must be the same permalink



## Authors

Timo Niessner


## Version History

* 2.00
    * Various bug fixes and optimizations
    * See [commit change]() or See [release history]()
* 1.00
    * Initial Release

## License

Free to Copy&Paste just do it

## Acknowledgments

wordpress api 
