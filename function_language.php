<?php
function btn_lang_flag($LANG_SLUG , $IMG_URL) {

	//get post detail
	global $post;
	$lang_link = "/";

	//get perent post id first
	$page_parent_id = $post->post_parent;

	//get parent slug by page id
	$page_parent = get_post_field( 'post_name', $page_parent_id );

	//get post slug
    $page_slug = $post->post_name;

	if ($page_parent_id != 0) {
		//parent slug found
		//$lang_link = '/'.$LANG_SLUG.'/'.$page_parent.'/'.$page_slug;
		$lang_link = '/'.$LANG_SLUG.'/'.$page_slug;
		
	} else {
		//main parent slug
		$lang_link = '/'.$LANG_SLUG;
	}
	return "<a href='$lang_link'><img alt='' src='$IMG_URL'></a>";
}

?>